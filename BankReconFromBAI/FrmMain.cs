﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace BankReconFromBAI
{
    // Wells Fargo version
    public partial class FrmMain : Form
    {
        private string _fyleIn = string.Empty,
            _fyleInCopy = string.Empty,
            _fyleOut = string.Empty,
            _fyleCbc = string.Empty,
            _fyleOutCopy = string.Empty,
            _fyleCbcCopy = string.Empty,
            _fyleTotals = string.Empty,
            //_destFolder = @"\\amidala\store\Accounting_InFor Solution\Documentation\ZAX LLC Bank Statements\10003 Wells Operating 4374\",
            _cashCode = "4374",
            _bank="WellsFargo",
            //_i4Acct="10000-4374",        //13002-4374
            _bankId = @"121000248",
            _operAcct = @"4124504374",
            _pmtCode = @"CHK",
            _errSql= string.Empty;
        //_bnkAmtSign=@"+",
        //_status="R";

        private int _recAll16Count = 0;
        private decimal _recAll16Amt = 0;

        private void cmdOutputBrowse_Click(object sender, EventArgs e)
        {
            //SaveFileDialog dlgFyleOut = new SaveFileDialog
            //{
            //    RestoreDirectory = true,
            //    Filter = @"Text files (*.txt)|*.txt"
            //};
            //if(dlgFyleOut.ShowDialog()==DialogResult.OK)
            //{
            //    _fyleOut = dlgFyleOut.FileName.Trim();
            //    tbxOutFile.Text = _fyleOut;
            //}
            //CheckIfOkay();
        }

        public FrmMain()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
                                    Application.Exit();
        }

        private void cmdBrowse_Click(object sender, EventArgs e)
        {
            //OpenFileDialog dlgFyleIn = new OpenFileDialog
            //{
            //    RestoreDirectory = true,
            //    Filter = @"All files (*.*)|*.*"
            //};
            //if (dlgFyleIn.ShowDialog() == DialogResult.OK)
            //{
            //    _fyleIn = dlgFyleIn.FileName.Trim();
            //    tbxFile.Text = _fyleIn;
            //}
            //CheckIfOkay();
        }

        //private void CheckIfOkay()
        //{
        //    //            cmdCreate185.Enabled = (tbxFile.Text.Trim().Length > 0 && tbxOutFile.Text.Trim().Length > 0) ? true : false;
        //    cmdCreate185.Enabled = tbxFile.Text.Trim().Length > 0 ? true : false;
        //}

        private void connectToDMZ()
        {
            System.Diagnostics.Process objProcess = new System.Diagnostics.Process();
            objProcess.StartInfo.FileName = "connect.bat";
            objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;    // to hide the command window popping up
            objProcess.Start();
            objProcess.WaitForExit();    // Gives time for the process to complete operation.
                                         // After code is executed, call the dispose() method
            objProcess.Dispose();
        }



        private void FrmMain_Load(object sender, EventArgs e)
        {
            _errSql = $@"C:\Infor\Data\BAI Files\errors_{_cashCode}_{DateTime.Now.Month}_{DateTime.Now.Day}.txt";
            connectToDMZ();
            // testing
            //    tbxFile.Text = @"C:\Infor\Data\BAI Files\P3 2016 WF Short Bank Statement.bai2";
            //    _fyleIn = tbxFile.Text;
            //    //tbxOutFile.Text = @"C:\Infor\Data\BAI Files\test.txt";
            //    _fyleOut = string.Empty;
            //    // testing
            //    CheckIfOkay();
            //    tlTip.SetToolTip(this.tbxFile, @"Enter or select via the Browse button the path and name of the bank file (*.bai) to be loaded.");
            //    tlTip.SetToolTip(this.tbxOutFile,@"Enter or select via the Browse button the path and name of the file to be created for loading purposes.");
            //    tlTip.SetToolTip(this.cmdCreate185, @"Click here to read the selected input file and create the defined output file.");
            //}

            //private void cmdCreate185_Click(object sender, EventArgs e)
            //{
            // time to work
            //Cursor = Cursors.WaitCursor;
            bool curTrans = false;
            bool useDBonly = false;     // for debuging after file in DB
            StringBuilder sbTransLyne=new StringBuilder(),
                sbSql=new StringBuilder();
            int count = 0,
                countInFyle=0;
            // per marc 7-20-2016 
            long transNumber = 0;
            //decimal dollarsInFyle = 0,
            //    tempDec=0;
            string issueDate = string.Empty;
            string[] fylesToLoad = Directory.GetFiles(Properties.Settings.Default.ftpPath);
            foreach (string fyle in fylesToLoad)
            {
                _fyleIn = fyle;
                FileInfo fInfo = new FileInfo(_fyleIn);
                DateTime dtFyleCreate = fInfo.CreationTime;
                //_fyleCbc = $@"C:\Infor\Data\BAI Files\CB500_{_cashCode}_{_bank}_{dtFyleCreate.Month}_{dtFyleCreate.Day}.csv";
                //_fyleOut = $@"C:\Infor\Data\BAI Files\CB185_{_cashCode}_{_bank}_{dtFyleCreate.Month}_{dtFyleCreate.Day}.txt";
                //_fyleTotals = $@"C:\Infor\Data\BAI Files\BAI_Totals_{_cashCode}_{_bank}_{dtFyleCreate.Month}_{dtFyleCreate.Day}.txt";
                //tbxOutFile.Text = _fyleCbc;
                //tbxOutFile.Update();
                StringBuilder sbMsg = new StringBuilder();

                string reconDte = string.Empty;     // $"{DateTime.Now.Year}{DateTime.Now.Month.ToString().Trim().PadLeft(2, '0')}{DateTime.Now.Day.ToString().Trim().PadLeft(2, '0')}";
                try
                {
                    //if (File.Exists(_fyleCbc))
                    //{
                    //    File.Delete(_fyleCbc);
                    //}
                    if (File.Exists(_errSql))
                    {
                        File.Delete(_errSql);
                    }
                    //if (File.Exists(_fyleOut))
                    //{
                    //    File.Delete(_fyleOut);
                    //}
                    //if (File.Exists(_fyleTotals))
                    //{
                    //    File.Delete(_fyleTotals);
                    //}
                    using (CodeSQL clsSql = new CodeSQL())
                    {
                        if (!useDBonly)
                        {
                            clsSql.CmdNonQuery(Properties.Settings.Default.aConCMS, "DELETE FROM [BaiDataWells]");
                            //clsSqlCe.ExecuteNonQuery("DELETE FROM [BaiDataWells]");
                            count = 0;
                            string[] inLynes = File.ReadAllLines(_fyleIn);
                            long curLyneCnt = 0;

                            while (curLyneCnt < inLynes.GetLength(0))
                            {
                                inLynes[curLyneCnt] = inLynes[curLyneCnt].EndsWith("/") ? inLynes[curLyneCnt].Substring(0, inLynes[curLyneCnt].Length - 1) : inLynes[curLyneCnt];
                                string[] fields = inLynes[curLyneCnt].Split(','); //lyne.Split(',');
                                switch (fields[0])
                                {
                                    case "02":
                                        issueDate =
                                            $"20{fields[4].Substring(0, 2)}{fields[4].Substring(2, 2)}{fields[4].Substring(4)}";
                                        reconDte = issueDate;
                                        // per marc 7-20-2016 
                                        transNumber = long.TryParse($"{fields[4].Substring(0, 2)}{fields[4].Substring(2, 2)}{fields[4].Substring(4)}0000", out transNumber) ? transNumber : 0;
                                        _fyleCbc = $@"C:\Infor\Data\BAI Files\CB500_{_cashCode}_{_bank}_{fields[4].Substring(2, 2)}_{fields[4].Substring(4)}.csv";
                                        _fyleOut = $@"C:\Infor\Data\BAI Files\CB185_{_cashCode}_{_bank}_{fields[4].Substring(2, 2)}_{fields[4].Substring(4)}.txt";
                                        _fyleTotals = $@"C:\Infor\Data\BAI Files\BAI_Totals_{_cashCode}_{_bank}_{fields[4].Substring(2, 2)}_{fields[4].Substring(4)}.txt";
                                        if (File.Exists(_fyleOut))
                                        {
                                            File.Delete(_fyleOut);
                                        }
                                        if (File.Exists(_fyleCbc))
                                        {
                                            File.Delete(_fyleCbc);
                                        }
                                        if (File.Exists(_fyleTotals))
                                        {
                                            File.Delete(_fyleTotals);
                                        }
                                        break;
                                    case "16":
                                        //start new trans
                                        _recAll16Count++;
                                        sbTransLyne.Clear();
                                        string chkNumber = string.Empty,
                                            lyne88 = string.Empty;
                                        //lblCount.Text = $"Transaction Count: {count}";
                                        //lblCount.Update();
                                        curTrans = true;
                                        lyne88 = string.Empty;
                                        while (curTrans)
                                        {
                                            if (curLyneCnt + 1 < inLynes.GetLength(0) &&
                                                inLynes[curLyneCnt + 1].Trim().Length > 0 &&
                                                inLynes[curLyneCnt + 1].StartsWith("88,"))
                                            {
                                                curLyneCnt++;
                                                lyne88 += inLynes[curLyneCnt].Trim();
                                                lyne88 = lyne88.Replace("'", "");
                                            }
                                            else
                                            {
                                                curTrans = false;
                                            }

                                        }
                                        if (fields[3].Trim().ToUpper() == "V" || fields[3].Trim().ToUpper() == "S")
                                        {
                                            chkNumber = fields[6].Trim();
                                        }
                                        else
                                        {
                                            chkNumber = fields[5].Trim();
                                        }
                                        chkNumber = chkNumber.Replace(@"/", "");
                                        chkNumber = chkNumber.Length < 10 ? chkNumber.PadLeft(10, '0') : chkNumber;
                                        while (chkNumber.Length > 10)
                                        {
                                            chkNumber = chkNumber.Substring(1);
                                        }
                                        count++;
                                        transNumber++;
                                        sbSql.Clear();
                                        sbSql.Append("INSERT INTO[BaiDataWells] ([rowCount],[BaiNbr],[TransNbr],[CheckNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88]) VALUES(");
                                        sbSql.Append($"{count}");
                                        sbSql.Append($",'{fields[1]}'");
                                        // per marc 7-20-2016           sbSql.Append($",'{chkNumber}'");
                                        sbSql.Append($",'{transNumber}'");    // per marc 7-20-2016 
                                        sbSql.Append($",'{chkNumber}'");
                                        sbSql.Append($",'{fields[2]}'");
                                        sbSql.Append($",'{reconDte}'");
                                        sbSql.Append($",'{issueDate}'");
                                        sbSql.Append($",'{lyne88}'");
                                        sbSql.Append(")");
                                        if (clsSql.CmdNonQuery(Properties.Settings.Default.aConCMS, sbSql.ToString()) != 1)
                                        {
                                            File.AppendAllText(_errSql, sbSql.ToString() + Environment.NewLine);
                                        }
                                        //if (clsSqlCe.ExecuteNonQuery(sbSql.ToString()) != 1)
                                        //{
                                        //    File.AppendAllText(_errSql, sbSql.ToString() + Environment.NewLine);
                                        //}
                                        curTrans = false;
                                            GC.Collect();
                                        break;
                                    case "88":
                                        break;
                                    default:
                                        break;
                                }
                                curLyneCnt++;
                            }
                        }
                        // check for dups & "0000000000" check numbers
                        GC.Collect();
                        // file totals here
                        string tmpBAI;
                        string cnt16 = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, "select count(*) from [BaiDataWells]").ToString();
                        string dollars16 = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, "select sum(cast([ReconBankAmt] as decimal (18,4))) from [BaiDataWells]").ToString();
                        //string cnt16 = clsSqlCe.ExecuteScalar("select count(*) from [BaiDataWells]");
                        //string dollars16 = clsSqlCe.ExecuteScalar("select sum(cast([ReconBankAmt] as decimal (18,4))) from [BaiDataWells]");
                        decimal sumAmt = decimal.TryParse(dollars16, out sumAmt) ? sumAmt : 0;
                        sumAmt = Math.Round(sumAmt / 100, 2);
                        _recAll16Count = int.TryParse(cnt16, out countInFyle) ? countInFyle : 0;
                        _recAll16Amt = sumAmt;
                        File.AppendAllText(_fyleTotals, $@"Issue Date: {issueDate.Substring(4,2)}/{issueDate.Substring(6)}/{issueDate.Substring(0,4)}    BAI_Totals{Environment.NewLine}");
                        File.AppendAllText(_fyleTotals, Environment.NewLine);
                        File.AppendAllText(_fyleTotals, $"Number of 16 records: {cnt16} for a total amount of {sumAmt:C}" + Environment.NewLine);
                        DataTable dtBai = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, "select distinct([BaiNbr]) from [BaiDataWells]").Tables[0];
                        //DataTable dtBai = clsSqlCe.GetDataset("select distinct([BaiNbr]) from [BaiDataWells]");
                        foreach (DataRow baiR in dtBai.Rows)
                        {
                            tmpBAI = baiR["BaiNbr"].ToString().Trim();
                            cnt16 = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select count(*) from [BaiDataWells] where [BaiNbr]='{tmpBAI}'").ToString();
                            dollars16 = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select sum(cast([ReconBankAmt] as decimal (18,4))) from [BaiDataWells] where [BaiNbr]='{tmpBAI}'").ToString();
                            //cnt16 = clsSqlCe.ExecuteScalar($"select count(*) from [BaiDataWells] where [BaiNbr]='{tmpBAI}'");
                            //dollars16 = clsSqlCe.ExecuteScalar($"select sum(cast([ReconBankAmt] as decimal (18,4))) from [BaiDataWells] where [BaiNbr]='{tmpBAI}'");
                            sumAmt = decimal.TryParse(dollars16, out sumAmt) ? sumAmt : 0;
                            sumAmt = Math.Round(sumAmt / 100, 2);
                            File.AppendAllText(_fyleTotals, $"BAI {tmpBAI} records: {cnt16} for a total amount of {sumAmt:C}{Environment.NewLine}" );
                        }
                        // end totals

                        string cntDistinct = "SELECT DISTINCT [TransNbr] FROM [BaiDataWells] where [BaiNbr]='475';",
                            cntTotal = "SELECT COUNT([TransNbr]) FROM [BaiDataWells] where [BaiNbr]='475';",
                            errChkNum = string.Empty;
                        long lngDistinct = 0,
                            lngTotal = 0;
                        lngTotal = long.TryParse(clsSql.CmdScalar(Properties.Settings.Default.aConCMS, cntTotal).ToString(), out lngTotal) ? lngTotal : 0;
                        lngDistinct = ((DataTable)clsSql.CmdDataset(Properties.Settings.Default.aConCMS, cntDistinct).Tables[0]).Rows.Count;
                        //lngTotal = long.TryParse(clsSqlCe.ExecuteScalar(cntTotal), out lngTotal) ? lngTotal : 0;
                        //lngDistinct = ((DataTable)clsSqlCe.GetDataset(cntDistinct)).Rows.Count;
                        while (lngTotal != lngDistinct)
                        {
                            DataTable dtDistTable = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, cntDistinct).Tables[0];
                            //DataTable dtDistTable = clsSqlCe.GetDataset(cntDistinct);
                            lblErrs.ForeColor = Color.Navy;
                            lblErrs.Text = @"Performing data inspection.";
                            lblErrs.Visible = true;
                            lblErrs.Update();
                            foreach (DataRow dRow in dtDistTable.Rows)
                            {
                                errChkNum = dRow["TransNbr"].ToString();
                                int numDupRows = int.TryParse(clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"SELECT COUNT([TransNbr]) FROM [BaiDataWells] where [BaiNbr]='475' and [TransNbr]='{errChkNum}'").ToString(), out numDupRows) ? numDupRows : 0;
                                //int numDupRows = int.TryParse(clsSqlCe.ExecuteScalar($"SELECT COUNT([TransNbr]) FROM [BaiDataWells] where [BaiNbr]='475' and [TransNbr]='{errChkNum}'"), out numDupRows) ? numDupRows : 0;
                                if (numDupRows > 1)
                                {
                                    //lblErrs.ForeColor = Color.Red;
                                    //lblErrs.Text = @"Errors found, checking for duplicate check numbers.";
                                    //lblErrs.Update();
                                    // fix dup 
                                    DataTable dtDupRowData = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, $"SELECT [rowCount],[TransNbr],[ReconBankAmt] FROM [BaiDataWells] where [BaiNbr]='475' and [TransNbr]='{errChkNum}'").Tables[0];
                                    //DataTable dtDupRowData = clsSqlCe.GetDataset($"SELECT [rowCount],[TransNbr],[ReconBankAmt] FROM [BaiDataWells] where [BaiNbr]='475' and [TransNbr]='{errChkNum}'");
                                    using (FrmFixCheckNum frmToFix = new FrmFixCheckNum())
                                    {
                                        frmToFix.DtDupChecks = dtDupRowData;
                                        frmToFix.ShowDialog();
                                        dtDupRowData = frmToFix.DtDupChecks;
                                        // update changes
                                        foreach (DataRow drUpdates in dtDupRowData.Rows)
                                        {
                                            sbSql.Clear();
                                            sbSql.Append("UPDATE [BaiDataWells]");
                                            sbSql.Append($" SET [TransNbr] ='{drUpdates["TransNbr"]}'");
                                            sbSql.Append($" WHERE [rowCount] = {drUpdates["rowCount"]}");
                                            clsSql.CmdNonQuery(Properties.Settings.Default.aConCMS, sbSql.ToString());
                                            //clsSqlCe.ExecuteNonQuery(sbSql.ToString());
                                        }
                                        frmToFix.Dispose();
                                        //Cursor = Cursors.WaitCursor;
                                    }
                                    GC.Collect();
                                    //this.Update();
                                }
                            }
                            lngTotal = long.TryParse(clsSql.CmdScalar(Properties.Settings.Default.aConCMS, cntTotal).ToString(), out lngTotal) ? lngTotal : 0;
                            lngDistinct = ((DataTable)clsSql.CmdDataset(Properties.Settings.Default.aConCMS, cntDistinct).Tables[0]).Rows.Count;
                            //lngTotal = long.TryParse(clsSqlCe.ExecuteScalar(cntTotal), out lngTotal) ? lngTotal : 0;
                            //lngDistinct = ((DataTable)clsSqlCe.GetDataset(cntDistinct)).Rows.Count;
                        }
                        // time to create the file
                        GC.Collect();
                        lblErrs.ForeColor = Color.Navy;
                        lblErrs.Text = @"Building CB500 input file.";
                        lblErrs.Visible = true;
                        string paymentCode,
                            tmp88,
                            tmp,
                            amt,
                            desc;
                        int seqNum = 0;
                        decimal sglAmt = 0,
                            sumAmt500 = 0;
                        DataTable dtToWrite = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, "Select [rowCount],[BaiNbr],[TransNbr],[CheckNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88] from [BaiDataWells];").Tables[0];
                        //DataTable dtToWrite = clsSqlCe.GetDataset("Select [rowCount],[BaiNbr],[TransNbr],[CheckNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88] from [BaiDataWells];");
                        //File.AppendAllText(_fyleCbc, @"CVC-RUN-GROUP,CVC-CASH-CODE,CVC-BANK-INST-CODE,CVC-TRANS-NBR,CVC-SEQ-NBR,CVC-COMPANY,CVC-VENDOR,CVC-REC-STATUS,CVC-SOURCE-CODE,CVC-ISSUE-DATE,CVC-ISSUE-BNK-AMT,CVC-ISSUE-BASE-AMT,CVC-RECON-DATE,CVC-RECON-BNK-AMT,CVC-RECON-BASE-AMT,CVC-DESCRIPTION,CVC-REFERENCE,CVC-DIS-ACCT-UNIT,CVC-DIS-ACCOUNT,CVC-DIS-SUB-ACCT,CVC-TAX-CODE,CVC-TRAN-TAXABLE,CVC-TRAN-TAX-AMT,CVC-JRNL-BOOK-NBR,CVC-ISSUE-TRAN-AMT,CVC-BNK-CNV-RATE,CVC-CURRENCY-CODE,CVC-BANK-ND,CVC-TRAN-ND,CVC-STMT-STATUS,CVC-PAY-GROUP,CVC-ORIG-CNV-RATE,CVC-SEGMENT-BLOCK,CVC-SOURCE,CVC-ACTIVITY,CVC-ACCT-CATEGORY,CVC-ANALYSIS-FLD,CVC-USER-FIELD1,CVC-USER-FIELD2,CVC-USER-FIELD3,CVC-USER-FIELD4,CVC-POST-DATE,CVC-DIS-COMPANY,CVC-DIS-SEG-BLOCK" + Environment.NewLine);
                        using (CodeGetTransactionDetails clsGetit = new CodeGetTransactionDetails())
                        {
                            foreach (DataRow drWrite in dtToWrite.Rows)
                            {
                                paymentCode = drWrite["BaiNbr"].ToString().Trim();
                                paymentCode = paymentCode == "475" ? _pmtCode : paymentCode;
                                paymentCode = paymentCode == "475" ? _pmtCode : paymentCode;
                                sbTransLyne.Clear();
                                sbTransLyne.Append(_bankId.PadRight(15, ' '));
                                sbTransLyne.Append(_operAcct.PadRight(35, ' '));
                                sbTransLyne.Append($" {paymentCode}");
                                string chkNumb = drWrite["TransNbr"].ToString().Trim().Replace("/", "");
                                if (paymentCode == _pmtCode)    // per marc 7/22/2016
                                {
                                    chkNumb = drWrite["CheckNbr"].ToString().Trim().Replace("/", "");
                                }
                                chkNumb = chkNumb.Length < 10 ? chkNumb.PadLeft(10, '0') : chkNumb;
                                while (chkNumb.Length > 10)
                                {
                                    chkNumb = chkNumb.Substring(1);
                                }
                                //chkNumb = chkNumb.TrimStart('0');
                                tmp = drWrite["ReconBankAmt"].ToString().Trim();
                                sbTransLyne.Append(chkNumb.PadLeft(10, ' '));
                                sbTransLyne.Append(drWrite["ReconBankAmt"].ToString().Trim().PadLeft(18, '0'));
                                sbTransLyne.Append($"+{drWrite["ReconDate"]}R");
                                if (paymentCode == _pmtCode)
                                {
                                    File.AppendAllText(_fyleOut, sbTransLyne + Environment.NewLine);
                                    clsSql.CmdNonQuery(Properties.Settings.Default.aConCMS, $"delete from [BaiDataWells] where [rowCount]={drWrite["rowCount"]}");
                                    //clsSqlCe.ExecuteNonQuery($"delete from [BaiDataWells] where [rowCount]={drWrite["rowCount"]}");
                                }
                                else
                                {
                                    // cbctrans csv
                                    amt = drWrite["ReconBankAmt"].ToString().TrimStart('0');
                                    sglAmt = decimal.TryParse(amt, out sglAmt) ? sglAmt : 0;
                                    sglAmt = Math.Round(sglAmt / 100, 2);
                                    sumAmt500 += sglAmt;
                                    amt = string.Format("{0:0.00}", sglAmt);
                                    seqNum++;

                                    tmp88 = drWrite["Data88"].ToString();
                                    tmp88 = tmp88.StartsWith("88,") ? tmp88.Substring(3) : tmp88;
                                    tmp88 = tmp88.Replace("88,", "|");
                                    string[] split88 = tmp88.Split('|');
                                    bool hasCom = (tmp88.ToUpper().Contains(".COM/") || tmp88.ToUpper().Contains(" COM/")) ? true : false;
                                    if (split88.Length > 1)
                                    {
                                        desc = split88.GetUpperBound(0) == 2 ? split88[2].Replace(@"/", "") : split88[1].Replace(@"/", "");
                                        desc = desc.Replace(",", "");
                                        desc = desc.Trim();
                                        desc = desc.Length > 30 ? desc.Substring(0, 30) : desc;
                                        if (hasCom && desc.Contains("PAYMENTECH DEPOSIT"))
                                        {
                                            desc = desc.Replace("PAYMENTECH DEPOSIT", "PAYMENTECH DEP COM");
                                            //if (desc.Length > 26)
                                            //{
                                            //    desc = $"{desc.Substring(0, 26)} COM";
                                            //}
                                            //else
                                            //{
                                            //    desc = $"{desc} COM";
                                            //}
                                        }
                                    }
                                    else
                                    {
                                        desc = string.Empty;
                                    }

                                    sbTransLyne.Clear();
                                    sbTransLyne.Append($"{DateTime.Now.Year}{DateTime.Now.Month.ToString().PadLeft(2, '0')}{DateTime.Now.Day.ToString().PadLeft(2, '0')}{_cashCode}");          //CVC - RUN - GROUP
                                    sbTransLyne.Append($",{_cashCode}"); //CVC - CASH - CODE
                                    sbTransLyne.Append($",{drWrite["BaiNbr"]}"); //CVC - BANK - INST - CODE
                                    tmp = split88[0].Replace("OTHER REFERENCE:", "");
                                    tmp = tmp.EndsWith("/") ? tmp.Substring(0, tmp.Length - 1) : tmp;
                                    tmp = tmp.Length > 10 ? tmp.Substring(tmp.Length - 10) : tmp;
                                    // per marc 7/20/2016   sbTransLyne.Append($",{tmp}"); //CVC - TRANS - NBR
                                    sbTransLyne.Append($",{chkNumb}"); //CVC - TRANS - NBR    // per marc 7/20/2016
                                    sbTransLyne.Append($",{seqNum}"); //CVC - SEQ - NBR
                                    sbTransLyne.Append(",200"); //CVC - COMPANY
                                    sbTransLyne.Append(","); //CVC - Vendor ?????
                                    sbTransLyne.Append(",2"); //CVC - REC - STATUS
                                    sbTransLyne.Append(",99"); //CVC - SOURCE - CODE
                                                               // per marc 6/13/2016   
                                    sbTransLyne.Append($",{drWrite["IssueDate"]}"); //CVC - ISSUE - DATE    // per marc 7/8/2016 
                                    // per marc 7/8/2016    sbTransLyne.Append($",{issueDate}"); //CVC - ISSUE - DATE
                                    sbTransLyne.Append($",{amt}"); //CVC - ISSUE - BNK - AMT
                                    sbTransLyne.Append($",{amt}"); //CVC - ISSUE - BASE - AMT
                                                                   // per marc 6/13/2016   
                                    sbTransLyne.Append($",{drWrite["ReconDate"]}"); //CVC - RECON - DATE        // per marc 7/8/2016 
                                    // per marc 7/8/2016    sbTransLyne.Append($",{issueDate}"); //CVC - RECON - DATE   // per marc 7/8/2016   
                                    sbTransLyne.Append($",{amt}"); //CVC - RECON - BNK - AMT
                                    sbTransLyne.Append($",{amt}"); //CVC-RECON-BASE-AMT
                                    sbTransLyne.Append($",{desc}"); //CVC - DESCRIPTION
                                    sbTransLyne.Append(","); //CVC-REFERENCE
                                    clsGetit.Rec88 = drWrite["Data88"].ToString().Trim();
                                    clsGetit.BaiCode = drWrite["BaiNbr"].ToString().Trim();
                                    clsGetit.UnScramble88();
                                    if (clsGetit.AcctUnit.Trim().Length == 0)
                                    {
                                        sbTransLyne.Append($",9999"); //CVC - DIS - ACCT - UNIT
                                    }
                                    else
                                    {
                                        sbTransLyne.Append($",{clsGetit.AcctUnit}"); //CVC - DIS - ACCT - UNIT
                                    }
                                    if (clsGetit.DistAcct.Trim().Length == 0)
                                    {
                                        sbTransLyne.Append($",99999"); //CVC - DIS - ACCOUNT
                                    }
                                    else
                                    {
                                        sbTransLyne.Append($",{clsGetit.DistAcct}"); //CVC - DIS - ACCOUNT
                                    }
                                    sbTransLyne.Append($",{clsGetit.SubAcct}"); //CVC - DIS - SUB - ACCT
                                    sbTransLyne.Append(","); //CVC-TAX-CODE,
                                    sbTransLyne.Append(","); //CVC-TRAN-TAXABLE
                                    sbTransLyne.Append(","); // CVC - TRAN - TAX - AMT
                                    sbTransLyne.Append(","); // CVC-JRNL-BOOK-NBR
                                    sbTransLyne.Append($",{amt}"); //CVC - ISSUE - TRAN - AMT
                                    sbTransLyne.Append(","); // CVC-BNK-CNV-RATE
                                    sbTransLyne.Append(","); // CVC-CURRENCY-CODE
                                    sbTransLyne.Append(","); // CVC-BANK-ND
                                    sbTransLyne.Append(","); // CVC-TRAN-ND
                                    sbTransLyne.Append(","); // CVC-STMT-STATUS
                                    sbTransLyne.Append(","); //CVC-PAY-GROUP
                                    sbTransLyne.Append(","); // CVC-ORIG-CNV-RATE
                                    sbTransLyne.Append(",");       // CVC-SEGMENT-BLOCK
                                    sbTransLyne.Append(",");      // CVC-SOURCE
                                    sbTransLyne.Append(",");   // CVC-ACTIVITY
                                    sbTransLyne.Append(",");   //  CVC-ACCT-CATEGORY
                                    sbTransLyne.Append(",");   // CVC-ANALYSIS-FLD
                                    sbTransLyne.Append(",");   // CVC-USER-FIELD1
                                    sbTransLyne.Append(",");   // CVC-USER-FIELD2
                                    sbTransLyne.Append(",");   // CVC-USER-FIELD3
                                    sbTransLyne.Append(",");   // CVC-USER-FIELD4
                                    sbTransLyne.Append(",");   // CVC-POST-DATE
                                    sbTransLyne.Append(",");   // CVC-DIS-COMPANY
                                    sbTransLyne.Append(",");   // CVC-DIS-SEG-BLOCK

                                    if (clsGetit.DoNotLoad)
                                    {
                                        sbTransLyne.Clear();
                                    }
                                    else
                                    {
                                        File.AppendAllText(_fyleCbc, sbTransLyne + Environment.NewLine);
                                        //delete rec
                                        clsSql.CmdNonQuery(Properties.Settings.Default.aConCMS, $"delete from [BaiDataWells] where [rowCount]={drWrite["rowCount"]}");
                                        //clsSqlCe.ExecuteNonQuery($"delete from [BaiDataWells] where [rowCount]={drWrite["rowCount"]}");
                                    }
                                }
                            }
                            File.AppendAllText(_fyleTotals, Environment.NewLine);
                            File.AppendAllText(_fyleTotals, $"CB500 16 records: {seqNum} for a total amount of {sumAmt500:C}" + Environment.NewLine);
                            File.AppendAllText(_fyleTotals, "Not in CB500 or CB185 records: " + Environment.NewLine);

                            //select [BaiNbr],[TransNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88] from [BaiDataWells] order by[BaiNbr]
                            DataSet notLoaded = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, "select[BaiNbr],[TransNbr],[CheckNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88] from [BaiDataWells] order by[BaiNbr]");
                            //DataTable notLoaded = clsSqlCe.GetDataset("select[BaiNbr],[TransNbr],[CheckNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88] from [BaiDataWells] order by[BaiNbr]");
                            //if(notLoaded.Rows.Count>0)
                            //{
                            if (notLoaded.Tables.Count > 0 && notLoaded.Tables[0].Rows.Count > 0)
                            {

                                File.AppendAllText(_fyleTotals, $"[BaiNbr]|[TransNbr]|[CheckNbr]|[ReconBankAmt]|[ReconDate]|[IssueDate]|[Data88]{Environment.NewLine}");
                                //foreach(DataRow bdRow in notLoaded.Rows)
                                foreach (DataRow bdRow in notLoaded.Tables[0].Rows)
                                {
                                    File.AppendAllText(_fyleTotals, $"{bdRow["BaiNbr"]}|{bdRow["TransNbr"]}|{bdRow["CheckNbr"]}|{bdRow["ReconBankAmt"]}|{bdRow["ReconDate"]}|{bdRow["IssueDate"]}|{bdRow["Data88"]}{Environment.NewLine}");
                                }
                            }
                        }
                        curTrans = false;
                        GC.Collect();
                        //lblErrs.BackColor = Color.GhostWhite;
                        //lblErrs.Text = @"CB500 file complete.";

                        try
                        {
                            _fyleInCopy = checkExistingFyle(Properties.Settings.Default.pthSave + Path.GetFileName(_fyleIn)+".txt");
                            _fyleCbcCopy = checkExistingFyle(Properties.Settings.Default.pthCB500 + Path.GetFileName(_fyleCbc));
                            _fyleOutCopy = checkExistingFyle(Properties.Settings.Default.pthCB185 + Path.GetFileName(_fyleOut));
                            string fyleCB500toProd = Properties.Settings.Default.pthCB500Prod + $"CB500_{_cashCode}.csv";
                            string fyleCB185toProd = Properties.Settings.Default.pthCB185Prod + "QB685IN";
                            if (File.Exists(_fyleIn))
                            {
                                File.Copy(_fyleIn, _fyleInCopy, true);
                                File.Delete(_fyleIn);
                            }
                            if (File.Exists(_fyleCbc))
                            {
                                File.Copy(_fyleCbc, _fyleCbcCopy, true);
                                File.Copy(_fyleCbc, fyleCB500toProd, true);
                                sbMsg.AppendLine();
                                sbMsg.Append($"The CB500 file has been placed in {Properties.Settings.Default.pthCB500Prod}");
                                sbMsg.AppendLine();
                                File.Delete(_fyleCbc);
                            }
                            if (File.Exists(_fyleOut) && _fyleOut.Length>0)
                            {
                                File.Copy(_fyleOut, _fyleOutCopy, true);
                                File.Copy(_fyleOut, fyleCB185toProd, true);
                                sbMsg.AppendLine();
                                sbMsg.Append($"The CB5185 file has been placed in {Properties.Settings.Default.pthCB185Prod}");
                                sbMsg.AppendLine();
                                File.Delete(_fyleOut);
                            }
                        }
                        catch (Exception ex)
                        {
                            File.AppendAllText(_errSql, @"File copy error: " + ex.Message + Environment.NewLine);
                        }
                    }
                }
                catch (Exception ex)
                {
                    File.AppendAllText(_errSql, ex.Message + Environment.NewLine);
                    //Cursor = Cursors.Default;
                    return;
                }
                // send email
                using (CodeEmail clsEmail = new CodeEmail())
                {
//                    StringBuilder sbMsg = new StringBuilder();
                    sbMsg.AppendLine();
                    sbMsg.AppendLine();
                    string[] totalsFyle = File.ReadAllLines(_fyleTotals);
                    foreach (string mLyne in totalsFyle)
                    {
                        if (mLyne.Trim().Length > 0)
                        {
                            sbMsg.AppendLine($"{mLyne}{Environment.NewLine}");
                           // sbMsg.AppendLine();
                        }
                    }
                    if (File.Exists(_errSql))
                    {
                        string[] errors = File.ReadAllLines(_errSql);
                        sbMsg.AppendLine();
                        sbMsg.AppendLine("Errors from Bank BAI file.");
                        sbMsg.AppendLine();
                        foreach (string eLyne in errors)
                        {
                            sbMsg.AppendLine(eLyne);
                            sbMsg.AppendLine();
                        }
                    }
                    clsEmail.ExtMsg = sbMsg.ToString();
                    clsEmail.Bank = $"{_bank}({_cashCode})";
                    clsEmail.InFyle = Path.GetFileName(_fyleInCopy);
                    if(_fyleOutCopy.Trim().Length==0)
                    {
                        clsEmail.OutFyle = $"{Path.GetFileName(_fyleCbcCopy)}";
                    }
                    else
                    {
                        clsEmail.OutFyle = $"{Path.GetFileName(_fyleCbcCopy)} and to {Path.GetFileName(_fyleOutCopy)}";
                    }
                    clsEmail.SendEmail();
                }
                GC.Collect();
            }
            //Cursor = Cursors.Default;
            try
            {
                deleteOldFiles(Properties.Settings.Default.pthCB185, 90, "txt");
                deleteOldFiles(Properties.Settings.Default.pthCB500, 90, "csv");
                deleteOldFiles(Properties.Settings.Default.pthSave, 90, "*");
            }
            catch
            { }
            Application.Exit();
        }

        private string checkExistingFyle(string fyleToCheck)
        {
            string rslt = fyleToCheck;
            int cnt = 0;
            while(File.Exists(rslt))
            {
                cnt++;
                rslt = Path.GetDirectoryName(fyleToCheck) + $@"\V_{cnt}_" + Path.GetFileName(fyleToCheck);
            }

            return rslt;
        }

        private void deleteOldFiles(string folDer, int daysOld, string fyleExt)
        {
            try
            {
                //C:\inetpub\logs\LogFiles\W3SVC1  on naboo and balmorra
                DateTime today = DateTime.Now;
                DirectoryInfo drInfo = new DirectoryInfo(folDer);
                foreach (var dyrectory in drInfo.GetDirectories())
                {
                    string[] fyles = Directory.GetFiles(String.Format(@"{0}", folDer), $"*.{fyleExt}");
                    foreach (var fyle in fyles)
                    {
                        FileInfo fylInfo = new FileInfo(fyle);    //String.Format(@"{0}\{1}\{2}", dyrLog, dyrectory, fyle));
                        if ((today - fylInfo.CreationTime).Days > daysOld)                          //45 days for naboo and balmorra
                        {
                            File.Delete(fyle);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return;
        }

    }
}
