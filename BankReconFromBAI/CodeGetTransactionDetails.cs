﻿using System;


namespace BankReconFromBAI
{
    class CodeGetTransactionDetails : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        //properties
        public string Rec88 { set; get; }
        public string AcctUnit { set; get; }
        public string DistAcct { set; get; }
        public string SubAcct { set; get; }
        public string BaiCode { set; get; }
        public bool DoNotLoad { set; get; }
        
        //method
        public void UnScramble88()
        {
            int strtOfLast = 0;
            string tmp = string.Empty;
            DoNotLoad = false;
            try
            {
                SubAcct = string.Empty;
                AcctUnit = string.Empty;
                DistAcct = string.Empty;
                switch (BaiCode)
                {
                    case "169":
                        if (Rec88.ToUpper().Contains("ACH ORIGINATION"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "10099";
                        }

                        //==============AMB per Donna Tiller 5/15/2018====================
                       else if (Rec88.ToUpper().Contains("KIA MOTOR MANUF. VENDORPAY Zax LLC"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "13001";
                        }

                        else if (Rec88.ToUpper().Contains("UGA ACCT PAYABLE ACCTS"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "13001";
                        }

                        else if (Rec88.ToUpper().Contains("University of Ge DIRECT PAY"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "13001";
                        }
                        else if (Rec88.ToUpper().Contains("KIA MOTOR MANUF. VENDORPAY"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "13001";
                        }

                        //===================================================================


                        //levelup 169  AMB 6/25/2018
                        else if (Rec88.ToUpper().Contains("LEVELUP INC LU PAYMENT"))
                        {
                            strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast + 4);

                            DistAcct = "13000";           
                            SubAcct = "0500";



                            // AU??????
                            tmp = tmp.Replace(" SPND/", "");
                            string endstring = tmp.Substring(tmp.Length - 7);
                            strtOfLast = endstring.IndexOf(" ", StringComparison.Ordinal);
                            tmp = endstring.Substring(strtOfLast, endstring.Length - strtOfLast);
                            AcctUnit = tmp.Trim();
                            AcctUnit = AcctUnit.Replace("/", "");
                            tmp = tmp.Replace("LEVELUP INC LU PAYMENT", "");
                            tmp = Scrub88(tmp);
                            AcctUnit = Scrub88(tmp);
                        }





                        else if (Rec88.ToUpper().Contains("ACH REJECT ADJ")
                                 || Rec88.ToUpper().Contains("ACH RETURNS")
                                 
                                 )
                        {
                            DoNotLoad = true;
                           
                        }
                        else if (Rec88.ToUpper().Contains("AMERICAN EXPRESS CHGBCK/ADJ"))
                        {
                            DistAcct = "13000";
                            strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast + 4);
                            //strtOfLast = tmp.LastIndexOf(" ", StringComparison.Ordinal);
                            tmp = tmp.Substring(34).Trim().ToUpper();
                            AcctUnit = Scrub88(tmp);
                        }
                        else if (Rec88.ToUpper().Contains("AMERICAN EXPRESS SETTLEMENT"))
                        {
                            DistAcct = "13000";    //8/9/2016 per Sabrina
                            SubAcct = "0200";
                            // AU??????
                            strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast + 38).ToUpper().Trim();
                            tmp = tmp.ToUpper();
                            if (tmp.Contains("ZAX") && !tmp.Contains("ZAXBY"))
                            {
                                tmp = tmp.Substring(tmp.IndexOf("ZAX", StringComparison.Ordinal) + 3, 5);
                                AcctUnit = tmp.Trim();
                            }
                            else
                            {
                                if (tmp.TrimStart('0').Length > 5 && tmp.Contains("ZAXBYS #"))
                                {
                                    tmp = tmp.Substring(tmp.IndexOf("#", StringComparison.Ordinal));
                                    if (!tmp.Contains("ZAX"))
                                    {
                                        tmp = tmp.Substring(0, 6);
                                    }
                                }
                                strtOfLast = tmp.IndexOf(" ", StringComparison.Ordinal);
                                if (strtOfLast > 0)
                                {
                                    tmp = tmp.Substring(0, strtOfLast).Trim();
                                }
                                AcctUnit = Scrub88(tmp);
                            }
                        }
                        else if (Rec88.ToUpper().Contains("OFF-CAMPUS ADVAN DAILY SLMT"))
                        {
                            AcctUnit = "1063";
                            DistAcct = "13000";         //8/9/2016 per Sabrina
                            SubAcct = "0400";
                        }
                        else if (Rec88.ToUpper().Contains("PAYMENTECH CHARGEBACK"))
                        {
                            DistAcct = "51750";            //8/9/2016 per Sabrina
                            // AU??????
                            // per Marc 7/8/2016
                            //strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            //tmp = Rec88.Substring(strtOfLast + 4);
                            ////strtOfLast = tmp.LastIndexOf(" ", StringComparison.Ordinal);
                            //tmp = tmp.Substring(34).Trim().ToUpper();
                            //AcctUnit = Scrub88(tmp);

                            tmp = tmp.ToUpper();
                            tmp = tmp.Replace("PAYMENTECH CHARGEBACK", "");
                            tmp = Scrub88(tmp);
                            if (tmp.Contains("SUB ACCT"))
                            {
                                strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                SubAcct = tmp.Substring(strtOfLast + 8);
                            }
                            strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast + 4);
                            strtOfLast = tmp.LastIndexOf(" ", StringComparison.Ordinal);
                            tmp = tmp.Substring(strtOfLast).Trim().ToUpper();
                            AcctUnit = Scrub88(tmp);

                        }
                        else if (Rec88.ToUpper().Contains("PAYMENTECH DEPOSIT"))
                        {
                            DistAcct = "13000";           //8/9/2016 per Sabrina
                            SubAcct = "0100";
                            // AU??????
                            if (Rec88.ToUpper().Contains("ZAXBYS"))
                            {
                                strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                                tmp = Rec88.Substring(strtOfLast + 4);
                                tmp = tmp.Substring(34).Trim().ToUpper();
                            }
                            else if (Rec88.ToUpper().Contains("ZAX"))
                            {
                                strtOfLast = Rec88.IndexOf("ZAX", StringComparison.Ordinal);
                                tmp = Rec88.Substring(strtOfLast + 3);
                            }
                            AcctUnit = Scrub88(tmp);
                        }
                        else if (Rec88.ToUpper().Contains("PAYTRONIX CASH C&D")                                 //8/12/2016 redefined per Sabrina
                            || (Rec88.ToUpper().Contains("PAYTRONIX") && Rec88.ToUpper().Contains("CASH C&D")))  //8/24/2016 as sent by paytronix      
                        {
                            AcctUnit = "900000";
                            DistAcct = "13000";            //8/9/2016 per Sabrina
                            SubAcct = "0300";
                            if (Rec88.ToUpper().Contains("_001"))
                            {
                                strtOfLast = Rec88.LastIndexOf("_001");
                                tmp = Rec88.Substring(strtOfLast + 1);
                                AcctUnit = tmp.Substring(0, 5);
                            }
                            if (Rec88.ToUpper().Contains("ZAX1"))
                            {
                                strtOfLast = Rec88.LastIndexOf("ZAX1");
                                tmp = Rec88.Substring(strtOfLast + 3);
                                AcctUnit = tmp.Substring(0, 4);
                            }
                        }
                        //8/12/2016 per Sabrina     else if (Rec88.ToUpper().Contains("PAYTRONIX CASH C&D CORPORATE ACH CORPORATE ACH"))
                        //8/12/2016 per Sabrina     {
                        //8/12/2016 per Sabrina         AcctUnit = "900000";
                        //8/12/2016 per Sabrina         DistAcct = "13000";            //8/9/2016 per Sabrina
                        //8/12/2016 per Sabrina         SubAcct = "0300";
                        //8/12/2016 per Sabrina     }
                        //8/12/2016 per Sabrina     else if (Rec88.ToUpper().Contains("PAYTRONIX CASH C&D ZAX1"))
                        //8/12/2016 per Sabrina     {
                        //8/12/2016 per Sabrina         DistAcct = "13000";
                        //8/12/2016 per Sabrina         strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                        //8/12/2016 per Sabrina         tmp = Rec88.Substring(strtOfLast + 4);
                        //8/12/2016 per Sabrina         tmp = tmp.Replace("PAYTRONIX CASH C&D ZAX1", "1");
                        //8/12/2016 per Sabrina         AcctUnit = tmp.Substring(0, 4);
                        //8/12/2016 per Sabrina     }
                        //8/12/2016 per Sabrina     else if (Rec88.ToUpper().Contains("PAYTRONIX CASH C&D ZAX INC. ZAX INC."))
                        //8/12/2016 per Sabrina     {
                        //8/12/2016 per Sabrina         AcctUnit = "900000";
                        //8/12/2016 per Sabrina         DistAcct = "13000";
                        //8/12/2016 per Sabrina     }
                        else if (Rec88.ToUpper().Contains("THE COCA-COLA CO EDI PYMNT"))
                        {
                            AcctUnit = "999800";
                            DistAcct = "60070";
                        }
                        else if (Rec88.ToUpper().Contains("UGA ACCT PAYABLE ACCTS PAY"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "13000";                 //8/9/2016 per Sabrina
                            SubAcct = "0400";
                        }
                        else if (Rec88.ToUpper().Contains("VALDOSTA STATE U DIRECT PAY"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "13000";                 //8/9/2016 per Sabrina
                            SubAcct = "0400";
                        }
                        else if (Rec88.ToUpper().Contains("EAGLE MS BLACKBOARD"))     //9/19/2016 per Sabrina
                        {
                            AcctUnit = "114800";
                            DistAcct = "13000";
                            SubAcct = "0400";
                        }
                        else if (Rec88.ToUpper().Contains("ZFI PAY GROUP AP PAYMENT 100"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "13001";
                        }
                        else if (Rec88.ToUpper().Contains("UT VACH"))           //12/14/2016 per Donna and Sabrina
                        {
                            AcctUnit = "107900";
                            DistAcct = "13001";
                        }
                        break;
                    case "172":
                        if (Rec88.ToUpper().Contains("CASH VAULT - CURRENCY DIFFERENCE FR"))
                        {
                            DistAcct = "13002";     //9/29/2016 per Sabrina "51750";
                            SubAcct = "4374";       //9/29/2016 per Sabrina
                            // AU??????
                            // 7/13/2016 per Sabrina
                            //strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            //tmp = Rec88.Substring(strtOfLast + 4);
                            //tmp = tmp.Substring(34).Trim().ToUpper();
                            strtOfLast = Rec88.LastIndexOf("#", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast+1,4);
                            AcctUnit = Scrub88(tmp);
                        }
                        break;
                    case "195":
                        //                        if (Rec88.ToUpper().Contains("WT FED#00505 SYNOVUS BANK"))    per Marc 7/18/2016
                        if (Rec88.ToUpper().Contains("SYNOVUS BANK /ORG=ZAXBYS OPERATING"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "29850";    // per Sabrina 7/27/2016
                        }
                        break;
                    case "277":
                        DoNotLoad = true;
                        break;
                    case "301":
                        // has sub accts
                        if (Rec88.ToUpper().Contains("OVER THE COUNTER DEPOSIT"))
                        {
                            DistAcct = "13002";
                            SubAcct = "4374";
                            // AU??????
                            tmp = Rec88;
                            try
                            {
                                //tmp = tmp.Replace("OVER THE COUNTER DEPOSIT", "");
                                // per marc 6/28/2016
                                if (tmp.Contains("#"))
                                {
                                    strtOfLast = tmp.LastIndexOf("#");
                                    AcctUnit = tmp.Substring(strtOfLast, 5);
                                    AcctUnit = Scrub88(AcctUnit);
                                }
                                else if(tmp.Contains("ZAX"))
                                {
                                    strtOfLast = tmp.IndexOf("ZAX", StringComparison.Ordinal);
                                    AcctUnit = tmp.Substring(strtOfLast-6, 5);
                                    AcctUnit = Scrub88(AcctUnit);
                                }
                                tmp = Scrub88(tmp);
                                // per marc 6/30/2016          changed 7/19/2016 to 4374
                                //SubAcct = "13002-4374";
                                //if (tmp.Contains("SUB ACCT"))
                                //{
                                //    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                //    SubAcct = tmp.Substring(strtOfLast + 8);
                                //}
                                //strtOfLast = tmp.IndexOf(" ", StringComparison.Ordinal);
                                //tmp = tmp.Substring(0, strtOfLast);
                                //AcctUnit = tmp;
                            }
                            catch (Exception)
                            {
                                AcctUnit = tmp;
                            }
                        }
                        else if (Rec88.ToUpper().Contains("POST VERIFY DEPOSIT FR"))        // moved before "DEPOSIT FR" marc 8/2/2016
                        {
                            DistAcct = "13002";
                            SubAcct = "4374";
                            // AU??????
                            strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast + 4);
                            try
                            {
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("POST VERIFY DEPOSIT FR", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                                strtOfLast = tmp.IndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(0, strtOfLast);
                                AcctUnit = tmp;
                            }
                            catch (Exception)
                            {
                                AcctUnit = tmp;
                            }
                        }
                        else if (Rec88.ToUpper().Contains("DEPOSIT FR"))
                        {
                            DistAcct = "13002";
                            SubAcct = "4374";
                            // AU??????
                            strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast + 4);
                            try
                            {
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("DEPOSIT FR", "");
                                tmp = Scrub88(tmp);
                                // per marc 6/13/2016
                                //if (tmp.Contains("SUB ACCT"))
                                //{
                                //    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                //    SubAcct = tmp.Substring(strtOfLast + 8);
                                //}
                                strtOfLast = tmp.IndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(0, strtOfLast);
                                AcctUnit = tmp;
                            }
                            catch (Exception)
                            {
                                AcctUnit = tmp;
                            }
                        }
                        else if(Rec88.ToUpper().Contains("DESKTOP CHECK DEPOSIT"))
                        {
                            // per marc 6/25/2016
                            //DoNotLoad = true;
                            DistAcct = "13002";
                            SubAcct = "9999";
                            AcctUnit = "900000";
                            break;
                        }
                        else if (Rec88.ToUpper().Contains("EDEPOSIT IN BRANCH"))
                        {
                            DistAcct = "13002";
                            SubAcct = "4374";
                            // AU??????
                            strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast + 4);
                            try
                            {
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("EDEPOSIT IN BRANCH", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                                strtOfLast = tmp.IndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(0, strtOfLast);
                                AcctUnit = tmp;
                            }
                            catch (Exception)
                            {
                                AcctUnit = tmp;
                            }
                        }
                        break;

                    case "366":
                        if (Rec88.ToUpper().Contains("SAFEVANTAGE DEPOSIT FR"))
                        {
                            DistAcct = "13002";
                            SubAcct = "4374";
                            // AU??????
                            strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                            tmp = Rec88.Substring(strtOfLast + 4);
                            try
                            {
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("SAFEVANTAGE DEPOSIT FR", "");
                                tmp = Scrub88(tmp);
                               // if (tmp.Contains("SUB ACCT"))
                                //{
                                  //  strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    //SubAcct = tmp.Substring(strtOfLast + 8);
                                //}
                                strtOfLast = tmp.IndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(0, strtOfLast);
                                AcctUnit = tmp;
                            }
                            catch (Exception)
                            {
                                AcctUnit = tmp;
                            }
                        }
                        break;

                    case "469":
                        // AU??????
                        strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                        tmp = Rec88.Substring(strtOfLast + 4);
                        try
                        {
                            if (Rec88.ToUpper().Contains("AL-DEPT OF REV DIRECT DBT"))
                            {
                                DistAcct = "20260";
                                AcctUnit = "900000";
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }
                            else if (Rec88.ToUpper().Contains("AL ONESPOT TAX"))
                            {
                                DistAcct = "20260";
                                AcctUnit = "900000";
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }

                            else if (Rec88.ToUpper().Contains("TN STATE REVENUE "))
                            {
                                DistAcct = "20260";
                                AcctUnit = "900000";
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }

                            else if (Rec88.ToUpper().Contains("CITY OF B R SALES TAX"))
                            {
                                DistAcct = "20260";
                                AcctUnit = "900000";
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }



                            else if (Rec88.ToUpper().Contains("EAGLE MS BLACKBOARD"))     //10/10/2016 per Sabrina
                            {
                                AcctUnit = "114800";
                                DistAcct = "51705";
                                //SubAcct = "0400";                  //10/10/2016 per Sabrina
                            }
                            else if (Rec88.ToUpper().Contains("AL ONESPOT TAX ADOR"))
                            {
                                DistAcct = "20260";
                                AcctUnit = "900000";
                            }
                           
                            else if (Rec88.ToUpper().Contains("AMERICAN EXPRESS AXP DISCNT"))
                            {
                                DistAcct = "51705";
                                tmp = tmp.ToUpper();
                                strtOfLast = tmp.IndexOf("ZAX", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast -6, 5);
                                AcctUnit = tmp.Trim();
                            }
                            else if (Rec88.ToUpper().Contains("AMERICAN EXPRESS CHGBCK/ADJ"))
                            {
                                DistAcct = "51750";                   //8/9/2016 per Sabrina
                                tmp = tmp.ToUpper();
                                // per marc 6/15/2016
                                strtOfLast = tmp.IndexOf("ZAX", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast+3, 5);
                                AcctUnit = tmp.Trim();
                                tmp = tmp.Replace("AMERICAN EXPRESS CHGBCK/ADJ", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }
                            else if (Rec88.ToUpper().Contains("AMERICAN EXPRESS COLLECTION"))
                            {
                                DistAcct = "51705";
                                tmp = tmp.ToUpper();
                                // per marc 6/15/2016
                                strtOfLast = tmp.IndexOf("ZAX", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast + 3, 5);
                                AcctUnit = tmp.Trim();
                                tmp = tmp.Replace("AMERICAN EXPRESS COLLECTION", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }
                            else if (Rec88.ToUpper().Contains("COBR SALES TAX SALES TAX")
                                || Rec88.ToUpper().Contains("FLA DEPT REVENUE")
                                || Rec88.ToUpper().Contains("GEORGIA ITS TAX GA TX PYMT")
                                || Rec88.ToUpper().Contains("LIVINGSTON PARIS SALES TAX")
                                || Rec88.ToUpper().Contains("MY ALABAMA TAXES PAYMENT")
                                || Rec88.ToUpper().Contains("NC DEPT REVENUE TAX PYMT")
                                || Rec88.ToUpper().Contains("MECKLENBURG CO B ACH")
                                || Rec88.ToUpper().Contains("SC DEPT REVENUE DEBIT")
                                || Rec88.ToUpper().Contains("TENN DEPT OF REV SALES TAX")
                                || Rec88.ToUpper().Contains("STATE OF LOUISIA EPOSPYMNTS")
                                || Rec88.ToUpper().Contains("VA DEPT TAXATION TAX PAYMEN")
                                || Rec88.ToUpper().Contains("AR DFA REVENUE PAYMENT ")
                                || Rec88.ToUpper().Contains("WEBFILE TAX PYMT")
                                
                                )
                            {
                                DistAcct = "20260";
                                AcctUnit = "900000";
                            }
                            else if (Rec88.ToUpper().Contains("MERCHANT LINK MNTHLY FEE"))
                            {
                                DistAcct = "51705";
                                AcctUnit = "900000";
                            }
                            else if (Rec88.ToUpper().Contains("OFF-CAMPUS ADVAN FEE"))
                            {
                                DistAcct = "51705";
                                AcctUnit = "1063";
                            }
                            else if (Rec88.ToUpper().Contains("PAYMENTECH CHARGEBACK"))
                            {
                                DistAcct = "51750";     // per sabrina 8/26/2016     "51705";
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("PAYMENTECH CHARGEBACK", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                                //per Marc 7/25/2016
                                strtOfLast = Rec88.ToUpper().LastIndexOf("ZAXBYS", StringComparison.Ordinal);
                                tmp = Rec88.Substring(strtOfLast + 6,5);
                                AcctUnit = Scrub88(tmp);
                            }
                            else if (Rec88.ToUpper().Contains("PAYMENTECH DEPOSIT"))
                            {
                                DistAcct = "51705";
                                tmp = tmp.ToUpper();
                                strtOfLast = tmp.IndexOf("ZAXBYS", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast+6, 5);
                                AcctUnit = tmp.Trim();

                                tmp = tmp.Replace("PAYMENTECH DEPOSIT", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }
                            else if (Rec88.ToUpper().Contains("PAYMENTECH FEE"))
                            {
                                DistAcct = "51705";
                                tmp = tmp.ToUpper();
                                // per Marc, Sabrina 8/4/2016
                                strtOfLast = tmp.IndexOf("ZAXBYS", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast + 6, 5);
                                AcctUnit = tmp.Trim();
                                tmp = tmp.Replace("PAYMENTECH FEE", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }

                            //levelup 469  AMB
                            else if (Rec88.ToUpper().Contains("LEVELUP INC LU PAYMENT"))
                            {
                                DistAcct = "";
                                tmp = tmp.ToUpper();

                                //CodeSQL clsSQL = new CodeSQL();
                                //string chkLevelUpAcct = clsSQL.levelupDist(tmp);

                               // if (chkLevelUpAcct == "13000-500")
                                //{
                                 //   DistAcct = "13000";
                                   // SubAcct = "500";
                                //}
                               // else
                               // {
                                //    DistAcct = chkLevelUpAcct;
                                //    SubAcct = "";
                                //}

                                if (tmp.Contains("FEES"))
                                {
                                    DistAcct = "51705";
                                    SubAcct = "";
                                    tmp = tmp.Replace(" FEES", "");
                                }
                                if (tmp.Contains("CRDT"))
                                {
                                    DistAcct = "13000";
                                    SubAcct = "500";
                                    tmp = tmp.Replace(" CRDT", "");
                                }


                               
                                
                                string endstring = tmp.Substring(tmp.Length - 7, 7);
                                strtOfLast = endstring.IndexOf(" ", StringComparison.Ordinal);
                                tmp = endstring.Substring(strtOfLast, endstring.Length- strtOfLast);
                                AcctUnit = tmp.Trim();
                                AcctUnit = AcctUnit.Replace("/", "");
                                tmp = tmp.Replace("LEVELUP INC LU PAYMENT", "");
                                tmp = Scrub88(tmp);
                               
                            }


                            else if (Rec88.ToUpper().Contains("PAYTRONIX CASH C&D CORPORATE LOCAT"))
                            {
                                // per Sabrina 8/3/2016     DistAcct = "13000";
                                DistAcct = "51705";
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("PAYTRONIX CASH C&D CORPORATE LOCAT", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                                strtOfLast = tmp.IndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast, 5);
                                AcctUnit = tmp.Trim();
                            }
                            else if (Rec88.ToUpper().Contains("PAYTRONIXCASH CD CASH C&D"))
                            {
                                // paytronix change per Sabrina 10/13/2016
                                DistAcct = "51705";     // "13000";
                                //SubAcct = "0300";
                                strtOfLast = Rec88.ToUpper().LastIndexOf("ZAX");
                                tmp = Rec88.Substring(strtOfLast+3,4);
                                AcctUnit = tmp.Substring(0, 4);
                            }
                            else if (Rec88.ToUpper().Contains("PAYTRONIX CASH C&D ZAX1"))
                            {
                                DistAcct = "13000";
                                strtOfLast = Rec88.IndexOf("/88,", StringComparison.Ordinal);
                                tmp = Rec88.Substring(strtOfLast + 4);
                                tmp = tmp.Replace("PAYTRONIX CASH C&D ZAX1", "1");
                                AcctUnit = tmp.Substring(0, 4);
                            }
                            else if (Rec88.ToUpper().Contains("REGIONS AUTO PYMT")
                                || Rec88.ToUpper().Contains("ZAX PAY GROUP AP PAYMENT"))
                            {
                                DoNotLoad = true;
                            }
                            else if (Rec88.ToUpper().Contains("RSS OPERATING ACH DEBIT"))
                            {
                                DistAcct = "60300";
                                tmp = tmp.ToUpper();
                                //per marc 06/17/2016
                                strtOfLast = tmp.IndexOf("ZAX LLC", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast + 8, 5);
                                AcctUnit = tmp.Trim();
                                tmp = tmp.Replace("RSS OPERATING ACH DEBIT", "");
                                tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                            }
                            else if (Rec88.ToUpper().Contains("TENN DOR FETAX FE TAX PMT"))
                            {
                                DistAcct = "52300";
                                AcctUnit = "900000";
                            }
                            else if (Rec88.ToUpper().Contains("ZAXBYS ALBANY AD ACH DEBIT"))
                            {
                                DistAcct = "20272";
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("ZAXBYS ALBANY AD ACH DEBIT", "");
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                                strtOfLast = tmp.LastIndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast+1, 5);
                                AcctUnit = tmp.Trim();
                            }
                            else if (Rec88.ToUpper().Contains("ZAXBYS ATLANTA A ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS AUGUSTA A ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS BIRMINGHA ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS CHARLESTO ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS CHARLOTTE ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS COLUMBIA ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS FLORENCE- ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS GREATER G ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS GREEN-NEW ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS GREENSBOR ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS KNOXVILLE ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS MACON ADV ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS MONTGOMER ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS RALEIGH A ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS SAVANNAH ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS TALLAHASS ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS TRI-CITIE ACH DEBIT")
                                || Rec88.ToUpper().Contains("ZAXBYS ROANOKE-L ACH DEBIT")
                                )
                            {
                                DistAcct = "20272";
                                //tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                                strtOfLast = tmp.LastIndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast+1, 5);
                                AcctUnit = tmp.Trim();
                            }
                            else if (Rec88.ToUpper().Contains("ZAXBYS MULTI-DMA ACH DEBIT"))
                            {
                                DistAcct = "20272";
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("ZAXBYS MULTI-DMA ACH DEBIT", "");
                                //tmp = Scrub88(tmp);
                                if (tmp.Contains("SUB ACCT"))
                                {
                                    strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                    SubAcct = tmp.Substring(strtOfLast + 8);
                                }
                                strtOfLast = tmp.LastIndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast+1, 5);
                                AcctUnit = tmp.Trim();
                            }
                            else if (Rec88.ToUpper().Contains("ZAXBYS NATL MKT ACH DEBIT"))
                            {
                                DistAcct = "20271";
                                tmp = tmp.ToUpper();
                                //per marc06/16/2016
                                strtOfLast = tmp.IndexOf("ZAX LLC", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast + 8, 5);
                                AcctUnit = tmp.Trim();
                            }
                            else if (Rec88.ToUpper().Contains("ZAXBYS NPNN ADVE ACH DEBIT"))
                            {
                                DistAcct = "20272";
                                tmp = tmp.ToUpper();
                                tmp = tmp.Replace("ZAXBYS NPNN ADVE ACH DEBIT", "");
                                strtOfLast = tmp.LastIndexOf(" ", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast+1, 5);
                                AcctUnit = tmp.Trim();
                            }
                            else if (Rec88.ToUpper().Contains("ZAXBYS OPERATING ACH DEBIT"))
                            {
                                DistAcct = "20270";
                                tmp = tmp.ToUpper();
                                // per marc 6/6/2016
                                strtOfLast = tmp.IndexOf("ZAX LLC", StringComparison.Ordinal);
                                tmp = tmp.Substring(strtOfLast + 8, 5);
                                AcctUnit = tmp.Trim();
                            }

                           else if (Rec88.ToUpper().Contains("MECKLENBURG CO P")) //AMB 8/24/2017 per Donna Tiller
                            {
                                DoNotLoad = true;
                            }


                        }
                        catch (Exception)
                        {
                            AcctUnit = tmp.Trim();
                        }
                        break;
                    case "475":
                        DoNotLoad = true;
                        break;
                    case "477":
                        if (Rec88.ToUpper().Contains("BANK ORIGINATED DEBIT"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "51604";
                        }
                        break;
                    case "495":
                        if (Rec88.ToUpper().Contains("WT FED#")
                            && Rec88.ToUpper().Contains("Operating Company LLC"))
                        {
                            AcctUnit = "999800";
                            DistAcct = "20065";
                        }
                        else
                        {
                            DoNotLoad = true;
                        }
                        break;
                    case "506":
                        if (Rec88.ToUpper().Contains("WT SEQ")
                            && (Rec88.ToUpper().Contains("ZAX, INC. /BNF=ZAX INC PAYROLL")
                            || Rec88.ToUpper().Contains("ZAX, INC. /BNF=ZAX, INC. SRF#")
                            || Rec88.ToUpper().Contains("ZAX LLC /BNF=ZAX LLC SRF#"))  //AMB  5-8-2019  bank description changed to ZAX LLC on incoming BAI file
                            )
                        {
                            AcctUnit = "900000";
                            DistAcct = "10099";
                        }
                        else
                        {
                            DoNotLoad = true;
                        }
                        break;
                    case "566":
                        if (Rec88.ToUpper().Contains("RETURN ITEM CHARGE"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "13040";
                        }
                        break;
                    case "575":
                        DoNotLoad = true;
                        break;
                    case "629":
                        if (Rec88.ToUpper().Contains("TELLER-ADJ CASH LISTED INCORRECTLY")
                            || Rec88.ToUpper().Contains("TELLER-MISC ADJUSTMENT FR"))
                        {
                            DistAcct = "51750";
                            tmp = tmp.ToUpper();
                            tmp = tmp.Replace("TELLER-ADJ CASH LISTED INCORRECTLY", "");
                            tmp = tmp.Replace("TELLER-MISC ADJUSTMENT FR", "");
                            tmp = Scrub88(tmp);
                            if (tmp.Contains("SUB ACCT"))
                            {
                                strtOfLast = tmp.LastIndexOf("SUB ACCT", StringComparison.Ordinal);
                                SubAcct = tmp.Substring(strtOfLast + 8);
                            }
                            strtOfLast = tmp.IndexOf(" ", StringComparison.Ordinal);
                            tmp = tmp.Substring(strtOfLast, 5);
                            AcctUnit = tmp.Trim();
                        }
                        break;
                    case "666":
                        if (Rec88.ToUpper().Contains("TELLER-CHANGE ORDER DEBIT"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "10301";
                        }
                        break;
                    case "695":
                        if (Rec88.ToUpper().Contains("CASH VAULT - CURRENCY DIFFERENCE")
                            || Rec88.ToUpper().Contains("CASH VAULT - COIN DIFFERENCE"))
                        {
                            tmp =Rec88.ToUpper();
                            if (tmp.Contains("#"))
                            {
                                strtOfLast = tmp.LastIndexOf("#");
                                AcctUnit = tmp.Substring(strtOfLast, 5);
                            }
                            else if (tmp.Contains("ZAX,"))
                            {
                                strtOfLast = tmp.IndexOf("ZAX,", StringComparison.Ordinal);
                                AcctUnit = tmp.Substring(strtOfLast - 6, 5);
                            }
                            AcctUnit = Scrub88(AcctUnit);
                            // per Sabrina 1/17/2016
                            DistAcct = "13002";
                            SubAcct = "4374";
                        }
                        break;
                    case "698":
                        if (Rec88.ToUpper().Contains("CLIENT ANALYSIS SRVC CHRG"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "51700";
                        }
                        break;
                    case "699":
                        if (Rec88.ToUpper().Contains("WITHDRAWAL MADE IN A BRANCH/STORE"))
                        {
                            AcctUnit = "900000";
                            DistAcct = "10301";
                        }
                        break;
                    default:
                        break;
                }
                // clean up au just in case...
                if (!DoNotLoad)
                {
                    tmp = Scrub88(AcctUnit);
                    using (CodeSQL clsSql = new CodeSQL())
                    {
                        switch (tmp.Length)
                        {
                            case 1:
                                tmp = "0010" + tmp;
                                tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                                break;
                            case 2:
                                tmp = "001" + tmp;
                                tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                                break;
                            case 3:
                                tmp = "00" + tmp;
                                tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                                break;
                            case 4:
                                //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                                tmp = tmp + "00";
                                break;
                            case 5:
                                tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                                break;
                            default:
                                break;
                        }
                    }
                    AcctUnit = tmp;
                }
            }
            catch (Exception)
            {
                AcctUnit = tmp;
            }
            AcctUnit = AcctUnit.Trim().Length ==0 ? "900000" : AcctUnit.Trim();
        }
        private string Scrub88(string tmpIn)
        {
            string rslt = tmpIn.ToUpper();
            rslt = rslt.Contains("#0") ? rslt.Substring(rslt.IndexOf("#"), 6) : rslt;
            rslt = rslt.Contains("#1") ? rslt.Substring(rslt.IndexOf("#"), 5) : rslt;
            rslt = rslt.Replace(".COM", "");
            rslt = rslt.Replace("COM", "");
            rslt = rslt.EndsWith("/") ? rslt.Substring(0, rslt.Length - 1) : rslt;
            rslt = rslt.Replace("#", "");
            rslt = rslt.Replace("ZAXBY S", "");
            rslt = rslt.Replace("ZAXBYS", "");
            rslt = rslt.Replace("ZAX", "");
            rslt = rslt.Replace("INC", "");
            rslt = rslt.TrimStart('0');
            return rslt.Trim();
        }
    }
}
