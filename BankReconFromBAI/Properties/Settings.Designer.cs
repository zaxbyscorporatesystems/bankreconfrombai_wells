﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BankReconFromBAI.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\Infor\\Data\\BAI Files")]
        public string folderIn {
            get {
                return ((string)(this["folderIn"]));
            }
            set {
                this["folderIn"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\amidala\\store\\Accounting_InFor Solution\\Documentation\\ZAX LLC Bank Statements\\1" +
            "0000-4374 Wells Operating 4374\\10 CB500 PROD\\")]
        public string pthCB500 {
            get {
                return ((string)(this["pthCB500"]));
            }
            set {
                this["pthCB500"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\amidala\\store\\Accounting_InFor Solution\\Documentation\\ZAX LLC Bank Statements\\1" +
            "0000-4374 Wells Operating 4374\\20 CB185 PROD\\")]
        public string pthCB185 {
            get {
                return ((string)(this["pthCB185"]));
            }
            set {
                this["pthCB185"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\goober\\ftpusers\\external\\banks\\wellsfargo\\4374")]
        public string ftpPath {
            get {
                return ((string)(this["ftpPath"]));
            }
            set {
                this["ftpPath"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\amidala\\store\\Accounting_InFor Solution\\Documentation\\ZAX LLC Bank Statements\\1" +
            "0000-4374 Wells Operating 4374\\FTP Loaded\\")]
        public string pthSave {
            get {
                return ((string)(this["pthSave"]));
            }
            set {
                this["pthSave"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\balmorra\\QB650IN\\")]
        public string pthCB500Prod {
            get {
                return ((string)(this["pthCB500Prod"]));
            }
            set {
                this["pthCB500Prod"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("c:\\infor\\data\\temp\\")]
        public string pthCB185Local {
            get {
                return ((string)(this["pthCB185Local"]));
            }
            set {
                this["pthCB185Local"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\\\\balmorra\\QB685IN\\")]
        public string pthCB185Prod {
            get {
                return ((string)(this["pthCB185Prod"]));
            }
            set {
                this["pthCB185Prod"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("data source=dak;initial catalog=ZaxCMS;uid=sa;pwd=bbp8Lr923;")]
        public string aConCMS {
            get {
                return ((string)(this["aConCMS"]));
            }
            set {
                this["aConCMS"] = value;
            }
        }
    }
}
