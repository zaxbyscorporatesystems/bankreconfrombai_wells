﻿using System;
using System.Data.SqlClient;
using System.Data;


namespace BankReconFromBAI
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    class CodeSQL : IDisposable
    {
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Commands the scalar.
        /// </summary>
        /// <param name="con">The SQL connection.</param>
        /// <param name="sqlCmd">The SQL to execute.</param>
        /// <returns>The first column of the first datarow.</returns>
        public object CmdScalar(string con, string sqlCmd)
        {
            object rslt = 0;
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlQ = new SqlCommand())
                    {
                        sqlQ.Connection = sqlCon;
                        sqlQ.CommandText = sqlCmd;
                        rslt = sqlQ.ExecuteScalar();
                    }
                    sqlCon.Close();
                    sqlCon.Dispose();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }

        /// <summary>
        /// Commands the dataset.
        /// </summary>
        /// <param name="con">The SQL connection.</param>
        /// <param name="sqlCmd">The SQL command that defines the dataset to extract.</param>
        /// <returns>The dataset base upon the input command property.</returns>
        public DataSet CmdDataset(string con, string sqlCmd)
        {
            DataSet dsRslt = new DataSet();
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlDataAdapter sqlDap = new SqlDataAdapter())
                    {
                        sqlDap.SelectCommand = new SqlCommand(sqlCmd, sqlCon);
                        sqlDap.Fill(dsRslt);
                    }
                    sqlCon.Close();
                    sqlCon.Dispose();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return dsRslt;
        }
        /// <summary>
        /// Commands the non query.
        /// </summary>
        /// <param name="con">The SQL connection.</param>
        /// <param name="sqlCmd">The SQL command.</param>
        /// <returns>The number of lines affected by the command.</returns>
        public int CmdNonQuery(string con, string sqlCmd)
        {
            int rslt = 0;
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlQ = new SqlCommand())
                    {
                        sqlQ.Connection = sqlCon;
                        sqlQ.CommandText = sqlCmd;
                        rslt = sqlQ.ExecuteNonQuery();
                    }
                    sqlCon.Close();
                    sqlCon.Dispose();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }


        public string levelupDist(string str88text)
        {
            string connstr = Properties.Settings.Default.aConCMS;
            SqlConnection sqlCon = new SqlConnection(connstr);
            string strResult = "";
            try
            {

                // 6/26/2019  added removal of new levelup code from string so astore num comes out right  AMB
                //str88text = str88text.Replace("/", ""); //'remove trailing slash'
                str88text = str88text.Replace(" FEES/", ""); //'remove trailing slash'
                str88text = str88text.Replace(" SPND/", ""); //'remove trailing slash'
                str88text = str88text.Replace(" CRDT/", ""); //'remove trailing slash'

                sqlCon.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlCon;
                cmd.CommandText = "BAI_LevelupAcct_sp";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@code88", str88text).Direction = System.Data.ParameterDirection.Input;

                SqlParameter pAcctNum = new SqlParameter();
                pAcctNum.SqlDbType = SqlDbType.VarChar;
                pAcctNum.Size = 10;
                pAcctNum.Direction = ParameterDirection.Output;
                pAcctNum.ParameterName = "@Accountnum";
                cmd.Parameters.Add(pAcctNum);

                cmd.ExecuteNonQuery();
                strResult= cmd.Parameters["@Accountnum"].Value.ToString();
                sqlCon.Close();
                sqlCon.Dispose();
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
            }


            return strResult;


        }



    }
}
